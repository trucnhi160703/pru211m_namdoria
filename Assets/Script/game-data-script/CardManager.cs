using UnityEngine;
using UnityEngine.SceneManagement;

public class CardManager : MonoBehaviour
{
    public int round = 1;

    private GameDataHandler gameDataHandler;
    private GameData gameData;

    void Start()
    {
        gameDataHandler = GetComponent<GameDataHandler>();
        gameData = gameDataHandler.LoadGameData();
        if (gameData == null)
        {
            Debug.Log(round);
        }
        if (round > gameData.currentRound)
        {
            GetComponent<SpriteRenderer>().color = Color.gray;
        }
    }

    private void OnMouseDown()
    {
        int currentRound = gameData.currentRound;
        if (round == 1 && round <= currentRound)
        {
            SceneManager.LoadScene("Map01");
        }
        else if (round == 2 && round <= currentRound)
        {
            SceneManager.LoadScene("Map02");
        }
        else if (round == 3 && round <= currentRound)
        {
            SceneManager.LoadScene("Map03");
        }
    }
}
