using System.IO;
using UnityEngine;

[System.Serializable]
public class GameData
{
    public int currentRound;
}

public class GameDataHandler : MonoBehaviour
{
    private string savePath;

    private void Awake()
    {
        savePath = Path.Combine(Application.persistentDataPath, "gameData.json");
    }

    public GameData LoadGameData()
    {
        if (File.Exists(savePath))
        {
            string json = File.ReadAllText(savePath);
            return JsonUtility.FromJson<GameData>(json);
        }
        else
        {
            // If the file doesn't exist, create a new default GameData.
            return new GameData { currentRound = 1 };
        }
    }

    public void SaveGameData(GameData gameData)
    {
        string json = JsonUtility.ToJson(gameData);
        File.WriteAllText(savePath, json);
        Debug.Log("luu roi ne" + gameData.currentRound);
    }
}
