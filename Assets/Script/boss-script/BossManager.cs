using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossManager : MonoBehaviour
{
    public float moveSpeed = 2;
    public string bodyName;
    public string bloodBarName;
    public bool isDeath = false;

    private int isAttack = 0;
    private float bloodBarSpeed;
    private Animator bloodBarAnimator;
    private Animator boss01Animator;
    private BossBodyManager bossBodyManager;
    private BloodBarScript bloodBarScript;
    private bool doneSpell = false;

    private void Start()
    {
        //Get blood bar
        Transform bloodBar = transform.Find(bloodBarName);
        if (bloodBar != null)
        {
            bloodBarAnimator = bloodBar.GetComponent<Animator>();
            bloodBarAnimator.speed = 0;
            bloodBarSpeed = bloodBarAnimator.speed;
            bloodBarScript = bloodBar.GetComponent<BloodBarScript>();
        }

        //Get char
        Transform boss01 = transform.Find(bodyName);
        if (boss01 != null)
        {
            boss01Animator = boss01.GetComponent<Animator>();
            bossBodyManager = boss01.GetComponent<BossBodyManager>();

        }

        Invoke("SpellToIdle", 1.0f);

        Invoke("IdleToWalk", 1.5f);

        Invoke("StartMoving", 1.6f);

    }

    // Update is called once per frame
    void Update()
    {
        if (bloodBarScript.isEmpty)
        {
            isDeath = true;
            bloodBarAnimator.speed = 0;
            Destroy(gameObject.GetComponent<Collider>());
            Destroy(gameObject, (float)1.3);
            if (isAttack <= 0)
            {
                boss01Animator.SetTrigger("walk-to-death");
            }
            else
            {
                boss01Animator.SetTrigger("attack-to-death");
            }
        }

        if (!bloodBarScript.isEmpty)
        {
            bloodBarAnimator.speed = bossBodyManager.bloodBarSpeed;
            if (isAttack <= 0 && doneSpell)
            {
                boss01Animator.SetTrigger("attack-to-walk");
                transform.position = transform.position + (Vector3.left * moveSpeed) * Time.deltaTime;
            }
            else
            {
                boss01Animator.SetTrigger("walk-to-attack");
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("phapsu-body")
        || collision.collider.CompareTag("elf-body")
        || collision.collider.CompareTag("fire-knight-body")
        || collision.collider.CompareTag("normal-knight-body")
        || collision.collider.CompareTag("castle")
        || collision.collider.CompareTag("witch"))
        {
            isAttack++;
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("phapsu-body")
        || collision.collider.CompareTag("elf-body")
        || collision.collider.CompareTag("fire-knight-body")
        || collision.collider.CompareTag("normal-knight-body")
        || collision.collider.CompareTag("castle")
        || collision.collider.CompareTag("witch"))
        {
            isAttack--;
        }
    }

    private void SpellToIdle()
    {
        boss01Animator.SetTrigger("spell-to-idle");
    }

    private void IdleToWalk()
    {
        boss01Animator.SetTrigger("idle-to-walk");
    }

    private void StartMoving()
    {
        doneSpell = true;
    }
}
