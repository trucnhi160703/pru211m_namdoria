﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpawner : MonoBehaviour
{
    public GameObject playerPrefab; // Prefab của player
    public float spawnInterval = 5f; // Thời gian giữa mỗi lần spam player

    private Vector3 spawnPosition; // Vị trí bắt đầu của player
    private bool hasSpawnedPlayer = false; // Đã spam player chưa

    void Start()
    {
        spawnPosition = transform.position; // Lấy vị trí bắt đầu từ đối tượng PlayerSpawner
        StartCoroutine(SpawnPlayer());
    }

    IEnumerator SpawnPlayer()
    {
        while (true)
        {
            yield return new WaitForSeconds(spawnInterval); // Chờ 5 giây

            if (!hasSpawnedPlayer)
            {
                // Tạo một player mới bằng cách sao chép prefab và đặt vị trí tại spawnPosition
                Instantiate(playerPrefab, spawnPosition, Quaternion.identity);
                hasSpawnedPlayer = true; // Đánh dấu đã spam player
            }
        }
    }
}