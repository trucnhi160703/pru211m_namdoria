﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireKnightScript : MonoBehaviour
{
    public float moveSpeed = 2;
    public float bloodBarDefaultSpeed = 0.5f;

    private float bloodBarSpeed;
    private bool isTouch = true;
    private Animator bloodBarAnimator;
    private Animator fireknightAnimator;
    private int currentCollisions = 0;
    private BloodBarScript bloodBarScript;

    private void Start()
    {
        //Get blood bar
        Transform bloodBar = transform.Find("HealthBar_fireknight");
        if (bloodBar != null)
        {
            bloodBarAnimator = bloodBar.GetComponent<Animator>();
            bloodBarSpeed = bloodBarAnimator.speed;
            bloodBarAnimator.speed = 0;
        }

        bloodBarScript = bloodBar.GetComponent<BloodBarScript>();

        //Get fireknight
        Transform fireknight = transform.Find("fireknight");
        if (fireknight != null)
        {
            fireknightAnimator = fireknight.GetComponent<Animator>();
        }
        isTouch = false;
    }
    void Update()
    {
        if (currentCollisions == 0)
        {
            bloodBarAnimator.speed = 0;
            isTouch = false;
        }
        else
        {
            bloodBarAnimator.speed = bloodBarSpeed;
            isTouch = true;
        }
        if (!isTouch)
        {
            transform.position = transform.position + (Vector3.right * moveSpeed) * Time.deltaTime;
        }

        if (bloodBarScript.isEmpty)
        {
            bloodBarAnimator.speed = 0;
            Destroy(gameObject.GetComponent<Collider>());
            fireknightAnimator.SetTrigger("attack-to-death");
            Destroy(gameObject, (float)1.0);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("enemy-body") && !bloodBarScript.isEmpty)
        {
            bloodBarSpeed = bloodBarDefaultSpeed;
            fireknightAnimator.SetTrigger("run-to-attack");
            currentCollisions++;
            bloodBarSpeed = currentCollisions * bloodBarSpeed;
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("enemy-body"))
        {
            fireknightAnimator.SetTrigger("attack-to-run");
            bloodBarSpeed = bloodBarSpeed * ((currentCollisions - 1) / currentCollisions);
            currentCollisions--;
        }
    }
}

