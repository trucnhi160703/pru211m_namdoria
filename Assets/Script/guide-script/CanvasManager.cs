﻿using UnityEngine;

public class CanvasManager : MonoBehaviour
{
    public Canvas[] instructionCanvases; // Mảng chứa các Canvas hướng dẫn
    private int currentIndex = 0; // Chỉ số của Canvas hiện tại

    private void Start()
    {
        // Ẩn tất cả các Canvas khi bắt đầu, ngoại trừ Canvas đầu tiên
        for (int i = 0; i < instructionCanvases.Length; i++)
        {
            if (i == 0)
            {
                instructionCanvases[i].enabled = true;
            }
            else
            {
                instructionCanvases[i].enabled = false;
            }
        }
    }

    private void Update()
    {
        // Kiểm tra khi người chơi click chuột
        if (Input.GetMouseButtonDown(0))
        {
            ShowNextCanvas();
        }
    }

    void ShowNextCanvas()
    {
        // Kiểm tra xem `currentIndex` có nằm trong phạm vi của mảng hay không
        if (currentIndex >= 0 && currentIndex < instructionCanvases.Length)
        {
            // Ẩn Canvas hiện tại
            instructionCanvases[currentIndex].enabled = false;

            // Tăng chỉ số để hiển thị Canvas tiếp theo
            currentIndex++;

            // Kiểm tra xem đã hiển thị hết tất cả Canvas chưa
            if (currentIndex < instructionCanvases.Length)
            {
                // Hiển thị Canvas tiếp theo
                instructionCanvases[currentIndex].enabled = true;
            }
            else
            {
            }
        }
    }
}
