using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EasyUI.Dialogs;

public class GameManager : MonoBehaviour
{
    public CastleManager castleManager;
    public DialogUI dialogUISetting;
    public DialogUI dialogUIVictory;
    public DialogUI dialogUILose;
    public bool isWin = false;
    public bool isLose = false;
    public AudioClip winGame;
    public AudioClip loseGame;
    public int round = 0;

    private GameObject boss;
    private bool isBossFound = false;
    private BossManager bossManager;
    private AudioSource audioSource;
    private GameDataHandler gameDataHandler;


    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        gameDataHandler = GetComponent<GameDataHandler>();
    }

    void Update()
    {
        if (!isWin && !isLose)
        {
            if (dialogUISetting.isPopup)
            {
                Time.timeScale = 0;
            }
            else
            {
                Time.timeScale = 1;
            }
            if (!castleManager.bloodBarScript.isEmpty)
            {
                if (!isBossFound)
                {
                    boss = GameObject.FindGameObjectWithTag("boss");
                    if (boss != null)
                    {
                        bossManager = boss.GetComponent<BossManager>();
                        isBossFound = true;
                    }
                }
                if (isBossFound)
                {
                    if (bossManager.isDeath)
                    {
                        isWin = true;

                        GameData gameData = gameDataHandler.LoadGameData();
                        if (gameData.currentRound <= round)
                        {
                            gameData.currentRound = round + 1;
                            Debug.Log("khuc nay chua luu" + gameData.currentRound);
                            gameDataHandler.SaveGameData(gameData);
                        }
                        Invoke("WinGame", 1.29f);
                        Debug.Log("win r ye");
                    }
                }
            }
            else
            {
                Time.timeScale = 0;
                isLose = true;
                dialogUILose.ShowPopup();
                PlayLoseSound();
                Debug.Log("thua cmnr");
            }

        }

    }

    private void PlayWinSound()
    {
        audioSource.clip = winGame;
        audioSource.Play();
    }

    private void PlayLoseSound()
    {
        audioSource.clip = loseGame;
        audioSource.Play();
    }

    private void WinGame()
    {
        Time.timeScale = 0;
        PlayWinSound();
        dialogUIVictory.ShowPopup();
    }
}
