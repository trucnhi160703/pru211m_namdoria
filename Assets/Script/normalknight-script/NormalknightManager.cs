using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NormalknightManager : MonoBehaviour
{
    public float moveSpeed = 2;

    private const float maxX = 2.8f;
    private bool isOutOfBound = false;
    private int isAttack = 0;
    private float bloodBarSpeed;
    private Animator bloodBarAnimator;
    private Animator normalknightAnimator;
    private BloodBarScript bloodBarScript;
    private NormalknightBodyManager normalknightBodyManager;


    private void Start()
    {

        //Get blood bar
        Transform bloodBar = transform.Find("HealthBar_normalknight");
        if (bloodBar != null)
        {
            bloodBarAnimator = bloodBar.GetComponent<Animator>();
            bloodBarAnimator.speed = 0;
            bloodBarSpeed = bloodBarAnimator.speed;
            bloodBarScript = bloodBar.GetComponent<BloodBarScript>();
        }

        //Get char
        Transform normalknight = transform.Find("normalknight");
        if (normalknight != null)
        {
            normalknightAnimator = normalknight.GetComponent<Animator>();
            normalknightBodyManager = normalknight.GetComponent<NormalknightBodyManager>();
        }
    }


    // Update is called once per frame
    void Update()
    {
        if (transform.position.x > maxX)
        {
            isOutOfBound = true;
            if (!bloodBarScript.isEmpty && isAttack <= 0)
            {
                normalknightAnimator.SetTrigger("run-to-idle");
            }
        }

        if (bloodBarScript.isEmpty)
        {
            bloodBarAnimator.speed = 0;
            Destroy(gameObject.GetComponent<Collider>());
            Destroy(gameObject, (float)1.0);
            if (isAttack <= 0)
            {
                normalknightAnimator.SetTrigger(isOutOfBound ? "idle-to-death" : "run-to-death");
            }
            else
            {
                normalknightAnimator.SetTrigger("attack-to-death");
            }
        }
        else
        {
            bloodBarAnimator.speed = normalknightBodyManager.bloodBarSpeed;
            if (isAttack <= 0)
            {
                normalknightAnimator.SetTrigger(isOutOfBound ? "attack-to-idle" : "attack-to-run");
                if (!isOutOfBound)
                {
                    transform.position = transform.position + (Vector3.right * moveSpeed) * Time.deltaTime;
                }
            }
            else
            {
                normalknightAnimator.SetTrigger(isOutOfBound ? "idle-to-attack" : "run-to-attack");
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("skeleton-body") || collision.collider.CompareTag("boss-body"))
        {
            isAttack++;
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("skeleton-body") || collision.collider.CompareTag("boss-body"))
        {
            isAttack--;
        }
    }
}
