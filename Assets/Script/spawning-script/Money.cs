﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Money : MonoBehaviour
{
    public TMP_Text moneyText; // Gán Text UI trong Inspector
    public float moneyIncreaseRate = 1.0f; // Tốc độ tăng tiền mỗi giây
    public WitchCounter witchCounter;
    private float money = 0.0f;

    private void Start()
    {
        // Bắt đầu coroutine để tăng tiền sau mỗi 5 giây
        StartCoroutine(IncrementMoney());
    }

    private void Update()
    {
        foreach (var item in witchCounter.witchesList)
        {
            var witchManager = item.GetComponent<WitchManager>();
            if (witchManager.isSpawnCoin)
            {
                money += 1;
                UpdateMoneyText();
                witchManager.isSpawnCoin = false;
            }
        }
    }

    private IEnumerator IncrementMoney()
    {
        while (true)
        {
            yield return new WaitForSeconds(2.0f); // Chờ 5 giây
            money += moneyIncreaseRate; // Tăng tiền sau mỗi 5 giây
            UpdateMoneyText(); // Cập nhật hiển thị tiền trên màn hình
        }
    }

    private void UpdateMoneyText()
    {
        // Hiển thị tiền lên Text UI
        moneyText.text = "X " + money.ToString(); // Định dạng số tiền với hai chữ số thập phân
    }

    // Phương thức để giảm tiền
    public bool DecreaseMoney(float amount)
    {
        if (money >= amount)
        {
            money -= amount;
            UpdateMoneyText(); // Cập nhật hiển thị tiền trên màn hình
            return true; // Trả về true nếu giảm tiền thành công
        }
        else
        {
            return false; // Trả về false nếu không đủ tiền
        }
    }
}
