using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WitchCounter : MonoBehaviour
{
    // Tag name to filter objects by
    public string objectTag = "witch";
    public int numOfWitch = 0;
    public List<GameObject> witchesList = new List<GameObject>();

    void Start()
    {
        // Optionally, you can find and add any initial objects with the specified tag
        GameObject[] initialObjects = GameObject.FindGameObjectsWithTag(objectTag);
        witchesList.AddRange(initialObjects);
    }

    void Update()
    {
        // Check for new objects with the specified tag at regular intervals
        GameObject[] newObjects = GameObject.FindGameObjectsWithTag(objectTag);

        // Compare the newObjects array with the existing witchesList
        foreach (GameObject newObject in newObjects)
        {
            if (!witchesList.Contains(newObject))
            {
                // This is a new object; add it to the list
                witchesList.Add(newObject);

                // You can also perform any specific actions with the new object here
            }
        }

        // Update the witchesList as objects might get destroyed
        witchesList.RemoveAll(obj => obj == null);
        numOfWitch = witchesList.Count;
    }
}
