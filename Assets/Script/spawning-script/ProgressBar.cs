using UnityEngine;

public class ProgressBar : MonoBehaviour
{
    public int marker = 0;
    public float aniSpeed = 1f;
    private Animator animator;

    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    private void Update()
    {
        animator.speed = aniSpeed;
        AnimatorStateInfo stateInfo = animator.GetCurrentAnimatorStateInfo(0);
        if (stateInfo.normalizedTime > 0.3f && stateInfo.normalizedTime < 0.4f)
        {
            marker = 1;
        }
        if (stateInfo.normalizedTime > 0.6f && stateInfo.normalizedTime < 0.7f)
        {
            marker = 2;
        }

        if (stateInfo.normalizedTime >= 1f)
        {
            marker = 3;
        }

    }
}

