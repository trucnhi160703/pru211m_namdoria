using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnCard : MonoBehaviour
{
    public Money coin;
    public GameObject characterPrefab; // Assign the character prefab to spawn in the Inspector
    private Vector3 selectedPosition;
    public float value; //Number of coins need to spawn the character
    public Texture2D customCursorTexture;


    public CharacterSpawner characterSpawner;
    private bool isCardSelected = false;
    private Transform A, B, C, D;
    private void Start()
    {
        A = characterSpawner.topLeftCorner.transform;
        B = characterSpawner.topRightCorner.transform;
        C = characterSpawner.bottomRightCorner.transform;
        D = characterSpawner.bottomLeftCorner.transform;
    }
    private void OnMouseDown()
    {
        if (!isCardSelected && characterSpawner.isMouseDisable == false)
        {
            isCardSelected = true;
            Vector2 cursorHotspot = new Vector2(customCursorTexture.width / 2, customCursorTexture.height / 2);
            Cursor.SetCursor(customCursorTexture, cursorHotspot, CursorMode.ForceSoftware);
        }
        else if (isCardSelected)
        {
            isCardSelected = false;
            characterSpawner.isMouseDisable = false;
            Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
        }
    }

    private void Update()
    {
        if (isCardSelected)
        {
            characterSpawner.isMouseDisable = true;
        }
        if (Input.GetMouseButtonDown(1))
        {
            if (isCardSelected)
            {
                Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                selectedPosition = new Vector3(mousePosition.x, mousePosition.y, mousePosition.y);
                if (selectedPosition != null)
                {
                    if (!IsInsideTrapezoid(selectedPosition))
                    {
                        Debug.Log("Selected position is outside the allowed boundaries!");
                    }
                    else if (coin.DecreaseMoney(value))
                    {
                        // Spawn a new instance of the character prefab at the specified spawn point
                        var a = Instantiate(characterPrefab, selectedPosition, Quaternion.identity);
                        // Vector3 characterPosition = a.transform.position;
                        // characterPosition.z = characterPosition.y;
                        // a.transform.position = characterPosition;
                    }
                }
                else
                {
                    Debug.LogWarning("Spawn point not assigned!");
                }
            }
            isCardSelected = false;
            characterSpawner.isMouseDisable = false;
            Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
        }
    }

    bool IsInsideTrapezoid(Vector3 position)
    {
        float x = position.x;
        float y = position.y;

        float mAD = (A.position.y - D.position.y) / (A.position.x - D.position.x);
        float bAD = A.position.y - mAD * A.position.x;

        float mAB = (A.position.y - B.position.y) / (A.position.x - B.position.x);
        float bAB = A.position.y - mAB * A.position.x;

        float mBC = (B.position.y - C.position.y) / (B.position.x - C.position.x);
        float bBC = B.position.y - mBC * B.position.x;

        float mCD = (C.position.y - D.position.y) / (C.position.x - D.position.x);
        float bCD = C.position.y - mCD * C.position.x;

        float xM = (y - bAD) / mAD;
        float xN = (y - bBC) / mBC;
        float yP = mAB * x + bAB;
        float yQ = mCD * x + bCD;

        return (x >= xM && x <= xN && y >= yQ && y <= yP);
        // return (y <= yAB && y >= yCD);
    }
}

