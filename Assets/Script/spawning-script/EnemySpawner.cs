﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public GameObject skeletonPrefab; // Prefab của Skeleton
    public GameObject bossPrefab;
    public float defaultSpawnInterval = 3f;
    public float spawnInterval = 3f; // Thời gian giữa mỗi lần spam Skeleton
    public float fixedXPosition = 10f; // Trục X cố định
    public float minY = 1f; // Giá trị tối thiểu trên trục Y
    public float maxY = -2f; // Giá trị tối đa trên trục Y
    public ProgressBar progressBar;
    public bool isSpawning = true;
    public AudioClip skeletonMark;
    public AudioClip bossMark;

    private AudioSource audioSource;
    private int mark1 = 1;
    private int mark2 = 1;
    private int mark3 = 1;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        spawnInterval = defaultSpawnInterval;
        Invoke("StartSpawning", 10f);
    }
    private void Update()
    {
        if (progressBar.marker == 1 && mark1 == 1)
        {
            mark1++;
            spawnInterval = 1f;
            PlaySkeletonMark();
            Invoke("ResetRate", 20f);
        }
        else if (progressBar.marker == 2 && mark2 == 1)
        {
            mark2++;
            spawnInterval = 0.7f;
            PlaySkeletonMark();
            Invoke("ResetRate", 20f);
        }
        else if (progressBar.marker == 3 && mark3 == 1)
        {
            mark3++;
            SpawnBoss();
            spawnInterval = 0.7f;
            PlayBossMark();
            Invoke("ResetRate", 20f);
        }
    }

    private IEnumerator SpawnSkeleton()
    {
        while (isSpawning)
        {
            yield return new WaitForSeconds(spawnInterval);
            Vector3 spawnPosition = new Vector3(fixedXPosition, 0f, 0f);

            float randomY = Random.Range(minY, maxY);
            spawnPosition.y = randomY;
            spawnPosition.z = randomY;

            GameObject newSkeleton = Instantiate(skeletonPrefab, spawnPosition, Quaternion.identity);
        }
    }

    private void SpawnBoss()
    {
        Vector3 selectedPosition = new Vector3(8.02f, -0.71f, -0.71f);
        var a = Instantiate(bossPrefab, selectedPosition, Quaternion.identity);
    }

    private void ResetRate()
    {
        spawnInterval = defaultSpawnInterval;
    }

    private void StartSpawning()
    {
        StartCoroutine(SpawnSkeleton());
    }

    private void PlaySkeletonMark()
    {
        audioSource.clip = skeletonMark;
        audioSource.Play();
    }

    private void PlayBossMark()
    {
        audioSource.clip = bossMark;
        audioSource.Play();
    }
}
