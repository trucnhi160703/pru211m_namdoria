using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CastleManager : MonoBehaviour
{
    public float bloodBarDefaultSpeed = 0.5f;
    public BloodBarScript bloodBarScript;
    public float SkeletonDam = 0.1f;
    public float bossDam = 0.5f;

    private int numOfSkeleton = 0;
    private int numOfBoss = 0;
    private float bloodBarSpeed;
    private Animator bloodBarAnimator;
    private int currentCollisions = 0;

    private void Start()
    {
        bloodBarAnimator = bloodBarScript.GetComponent<Animator>();
        bloodBarSpeed = bloodBarAnimator.speed;
        bloodBarAnimator.speed = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (!bloodBarScript.isEmpty)
        {
            if (currentCollisions == 0)
            {
                bloodBarAnimator.speed = 0;
            }
            else
            {
                bloodBarAnimator.speed = bloodBarSpeed;
            }
        }
        else
        {
            bloodBarAnimator.speed = 0;
            Destroy(gameObject.GetComponent<Collider>());
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("skeleton") && !bloodBarScript.isEmpty)
        {
            numOfSkeleton++;
        }
        if (collision.collider.CompareTag("boss") && !bloodBarScript.isEmpty)
        {
            numOfBoss++;
        }
        currentCollisions = numOfBoss + numOfSkeleton;
        bloodBarSpeed = bloodBarDefaultSpeed + bloodBarDefaultSpeed * numOfSkeleton * SkeletonDam
                                             + bloodBarDefaultSpeed * numOfBoss * bossDam;
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("skeleton") && !bloodBarScript.isEmpty)
        {
            numOfSkeleton--;
        }
        if (collision.collider.CompareTag("boss") && !bloodBarScript.isEmpty)
        {
            numOfBoss--;
        }
        currentCollisions = numOfBoss + numOfSkeleton;
        bloodBarSpeed = bloodBarDefaultSpeed + bloodBarDefaultSpeed * numOfSkeleton * SkeletonDam
                                             + bloodBarDefaultSpeed * numOfBoss * bossDam;
    }
}
