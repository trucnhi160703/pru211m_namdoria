using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhapsuManager : MonoBehaviour
{
    public float moveSpeed = 2;

    private const float maxX = 2.8f;
    private bool isOutOfBound = false;
    private int isAttack = 0;
    private float bloodBarSpeed;
    private Animator bloodBarAnimator;
    private Animator phapsuAnimator;
    private BloodBarScript bloodBarScript;
    private PhapsuBodyManager phapsuBodyManager;


    private void Start()
    {

        //Get blood bar
        Transform bloodBar = transform.Find("HealthBar_phapsu2");
        if (bloodBar != null)
        {
            bloodBarAnimator = bloodBar.GetComponent<Animator>();
            bloodBarAnimator.speed = 0;
            bloodBarSpeed = bloodBarAnimator.speed;
            bloodBarScript = bloodBar.GetComponent<BloodBarScript>();
        }

        //Get char
        Transform phapsu = transform.Find("phapsu2");
        if (phapsu != null)
        {
            phapsuAnimator = phapsu.GetComponent<Animator>();
            phapsuBodyManager = phapsu.GetComponent<PhapsuBodyManager>();
        }
    }


    // Update is called once per frame
    void Update()
    {
        if (transform.position.x > maxX)
        {
            isOutOfBound = true;
            if (!bloodBarScript.isEmpty && isAttack <= 0)
            {
                phapsuAnimator.SetTrigger("run-to-idle");
            }
        }

        if (bloodBarScript.isEmpty)
        {
            bloodBarAnimator.speed = 0;
            Destroy(gameObject.GetComponent<Collider>());
            Destroy(gameObject, (float)1.0);
            if (isAttack <= 0)
            {
                phapsuAnimator.SetTrigger(isOutOfBound ? "idle-to-death" : "run-to-death");
            }
            else
            {
                phapsuAnimator.SetTrigger("attack-to-death");
            }
        }
        else
        {
            bloodBarAnimator.speed = phapsuBodyManager.bloodBarSpeed;
            if (isAttack <= 0)
            {
                phapsuAnimator.SetTrigger(isOutOfBound ? "attack-to-idle" : "attack-to-run");
                if (!isOutOfBound)
                {
                    transform.position = transform.position + (Vector3.right * moveSpeed) * Time.deltaTime;
                }
            }
            else
            {
                phapsuAnimator.SetTrigger(isOutOfBound ? "idle-to-attack" : "run-to-attack");
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("skeleton-body") || collision.collider.CompareTag("boss-body"))
        {
            isAttack++;
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("skeleton-body") || collision.collider.CompareTag("boss-body"))
        {
            isAttack--;
        }
    }
}
