using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WitchSpawnCoin : MonoBehaviour
{
    public bool isSpawnCoin = false;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(SpawnCoin());
    }

    public IEnumerator SpawnCoin(){
        while (true)
        {
            yield return new WaitForSeconds(5.0f); // Chờ 5 giây
            isSpawnCoin = true;
        }
    }
}
