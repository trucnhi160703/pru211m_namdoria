using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WitchManager : MonoBehaviour
{
    public float bloodBarDefaultSpeed = 0.5f;
    public float SkeletonDam = 0.1f;
    public float bossDam = 0.5f;
    public bool isSpawnCoin = false;

    private int numOfSkeleton = 0;
    private int numOfBoss = 0;
    private float bloodBarSpeed;
    private Animator bloodBarAnimator;
    private Animator witchAnimator;
    private int currentCollisions = 0;
    private BloodBarScript bloodBarScript;

    private void Start()
    {
        //Get blood bar
        Transform bloodBar = transform.Find("HealthBar_witch");
        if (bloodBar != null)
        {
            bloodBarAnimator = bloodBar.GetComponent<Animator>();
            bloodBarSpeed = bloodBarAnimator.speed;
            bloodBarAnimator.speed = 0;
            bloodBarScript = bloodBar.GetComponent<BloodBarScript>();
        }

        //Get char
        Transform witch = transform.Find("witch");
        if (witch != null)
        {
            witchAnimator = witch.GetComponent<Animator>();
        }

        StartCoroutine(SpawnCoin());
    }

    // Update is called once per frame
    void Update()
    {
        if (!bloodBarScript.isEmpty)
        {
            if (currentCollisions == 0)
            {
                bloodBarAnimator.speed = 0;
            }
            else
            {
                bloodBarAnimator.speed = bloodBarSpeed;
            }
        }
        else
        {
            bloodBarAnimator.speed = 0;
            Destroy(gameObject.GetComponent<Collider>());
            witchAnimator.SetTrigger("idle-to-death");
            Destroy(gameObject, (float)1.2);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("skeleton") && !bloodBarScript.isEmpty)
        {
            numOfSkeleton++;
        }
        if (collision.collider.CompareTag("boss") && !bloodBarScript.isEmpty)
        {
            numOfBoss++;
        }
        currentCollisions = numOfBoss + numOfSkeleton;
        bloodBarSpeed = bloodBarDefaultSpeed + bloodBarDefaultSpeed * numOfSkeleton * SkeletonDam
                                             + bloodBarDefaultSpeed * numOfBoss * bossDam;
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("skeleton") && !bloodBarScript.isEmpty)
        {
            numOfSkeleton--;
        }
        if (collision.collider.CompareTag("boss") && !bloodBarScript.isEmpty)
        {
            numOfBoss--;
        }
        currentCollisions = numOfBoss + numOfSkeleton;
        bloodBarSpeed = bloodBarDefaultSpeed + bloodBarDefaultSpeed * numOfSkeleton * SkeletonDam
                                             + bloodBarDefaultSpeed * numOfBoss * bossDam;
    }

    public IEnumerator SpawnCoin()
    {
        while (true)
        {
            yield return new WaitForSeconds(5.0f); // Chờ 5 giây
            isSpawnCoin = true;
        }
    }
}
