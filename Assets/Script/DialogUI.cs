﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace EasyUI.Dialogs
{

    public class DialogUI : MonoBehaviour
    {
        public GameObject dialogUI;
        public bool isPopup = false;

        private void Start()
        {
            // Ban đầu ẩn Popup khi Scene bắt đầu.
            dialogUI.SetActive(false);
        }

        public void ShowPopup()
        {
            // Bật Popup.
            dialogUI.SetActive(true);
            isPopup = true;
        }

        public void HidePopup()
        {
            // Tắt Popup.
            dialogUI.SetActive(false);
            isPopup = false;
        }
        public void SetActivePopup(bool isActive)
        {
            // Bật hoặc tắt Popup dựa trên giá trị của isActive.
            gameObject.SetActive(isActive);
        }
    }
}

