using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterSound : MonoBehaviour
{
    private AudioSource audioSource;
    public AudioClip attackSound;
    public AudioClip spellSound;
    public AudioClip deathSound;
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {

    }
    void PlayAttackSound()
    {
        audioSource.clip = attackSound;
        audioSource.Play();
    }

    void PlaySpellSound()
    {
        audioSource.clip = spellSound;
        audioSource.Play();
    }
    void PlayDeathSound()
    {
        audioSource.clip = deathSound;
        audioSource.Play();
    }
}
