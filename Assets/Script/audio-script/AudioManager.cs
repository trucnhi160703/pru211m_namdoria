using UnityEngine;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour
{
    public AudioMixer audioMixer; // Kết nối với Mixer trong Unity

    // Hàm để điều khiển âm lượng âm thanh music
    public void SetMusicVolume(float volume)
    {
        audioMixer.SetFloat("music", volume);
    }

    private void Awake()
    {
        // Đảm bảo rằng đối tượng AudioManager không bị hủy khi chuyển cảnh
        DontDestroyOnLoad(this.gameObject);
    }
}
