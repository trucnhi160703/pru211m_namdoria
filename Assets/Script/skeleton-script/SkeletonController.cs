﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkeletonController : MonoBehaviour
{
    private Animator animator;
    private bool isWalking = false;
    private bool isAttacking = false;
    private float distanceMoved = 0f; // Khoảng cách đã di chuyển
    public float moveSpeed = 3f; // Tốc độ di chuyển
    public float targetDistance = 10f; // Khoảng cách cần di chuyển

    void Start()
    {
        animator = GetComponent<Animator>();

        // Gọi hàm chuyển từ Idle sang Run sau 1 giây
        Invoke("StartRunning", 1f);
    }

    void Update()
    {
        // Kiểm tra nếu đã đạt được khoảng cách cần di chuyển
        if (distanceMoved < targetDistance)
        {
            // Tự động di chuyển Player1
            MovePlayer();
        }
        else
        {
            // Đạt đến khoảng cách cần di chuyển, chuyển sang trạng thái đánh
            StartAttacking();
        }
    }

    // Hàm để chuyển từ Idle sang Run
    void StartRunning()
    {
        // Đặt tham số "IsWalking" trong Animator Controller thành true để kích hoạt animation "Run"
        isWalking = true;
        animator.SetBool("IsWalking", isWalking);
    }

    // Hàm để chuyển sang trạng thái đánh
    void StartAttacking()
    {
        // Đặt tham số "IsAttacking" trong Animator Controller thành true để kích hoạt animation "Đánh"
        isAttacking = true;
        animator.SetBool("IsAttacking", isAttacking);
    }

    void MovePlayer()
    {
        // Di chuyển Player1 theo hướng từ phải sang trái (thay đổi transform.right thành -transform.right)
        Vector3 newPosition = transform.position + transform.right * moveSpeed * Time.deltaTime;
        transform.position = newPosition;

        // Cập nhật khoảng cách đã di chuyển
        distanceMoved += moveSpeed * Time.deltaTime;
    }
}
