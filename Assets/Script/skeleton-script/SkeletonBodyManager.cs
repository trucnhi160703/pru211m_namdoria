using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkeletonBodyManager : MonoBehaviour
{
    public float bloodBarDefaultSpeed = 0.8f;
    public float ArrowDam = 0.1f;
    public float PhapsuDam = 0.3f;
    public float FireKnightDam = 0.2f;
    public float NormalKnightDam = 0.2f;
    public float bloodBarSpeed;
    public int isHurt = 0;


    private int numOfArrow = 0;
    private int numOfPhapsu = 0;
    private int numofFireKnight = 0;
    private int numofNormalKnight = 0;

    private void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (isHurt <= 0)
        {
            bloodBarSpeed = 0;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("arrow"))
        {
            numOfArrow++;
        }
        if (collision.collider.CompareTag("phapsu"))
        {
            numOfPhapsu++;
        }
        if (collision.collider.CompareTag("fire-knight"))
        {
            numofFireKnight++;
        }
        if (collision.collider.CompareTag("normal-knight"))
        {
            numofNormalKnight++;
        }
        isHurt = numofFireKnight + numofNormalKnight + numOfPhapsu + numOfArrow;
        bloodBarSpeed = bloodBarDefaultSpeed + bloodBarDefaultSpeed * numOfArrow * ArrowDam
                                             + bloodBarDefaultSpeed * numOfPhapsu * PhapsuDam
                                             + bloodBarDefaultSpeed * numofFireKnight * FireKnightDam
                                             + bloodBarDefaultSpeed * numofNormalKnight * NormalKnightDam;
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("arrow"))
        {
            numOfArrow--;
        }
        if (collision.collider.CompareTag("phapsu"))
        {
            numOfPhapsu--;
        }
        if (collision.collider.CompareTag("fire-knight"))
        {
            numofFireKnight--;
        }
        if (collision.collider.CompareTag("normal-knight"))
        {
            numofNormalKnight--;
        }
        isHurt = numofFireKnight + numofNormalKnight + numOfPhapsu + numOfArrow;
        bloodBarSpeed = bloodBarDefaultSpeed + bloodBarDefaultSpeed * numOfArrow * ArrowDam
                                             + bloodBarDefaultSpeed * numOfPhapsu * PhapsuDam
                                             + bloodBarDefaultSpeed * numofFireKnight * FireKnightDam
                                             + bloodBarDefaultSpeed * numofNormalKnight * NormalKnightDam;
    }
}
