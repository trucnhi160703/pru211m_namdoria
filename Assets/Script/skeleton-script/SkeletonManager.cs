using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkeletonManager : MonoBehaviour
{
    public float moveSpeed = 2;

    private int isAttack = 0;
    private float bloodBarSpeed;
    private Animator bloodBarAnimator;
    private Animator skeletonAnimator;
    private BloodBarScript bloodBarScript;
    private SkeletonBodyManager skeletonBodyManager;

    private void Start()
    {

        //Get blood bar
        Transform bloodBar = transform.Find("HealthBar_skeleton");
        if (bloodBar != null)
        {
            bloodBarAnimator = bloodBar.GetComponent<Animator>();
            bloodBarAnimator.speed = 0;
            bloodBarSpeed = bloodBarAnimator.speed;
            bloodBarScript = bloodBar.GetComponent<BloodBarScript>();
        }

        //Get char
        Transform skeleton = transform.Find("skeleton");
        if (skeleton != null)
        {
            skeletonAnimator = skeleton.GetComponent<Animator>();
            skeletonBodyManager = skeleton.GetComponent<SkeletonBodyManager>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (bloodBarScript.isEmpty)
        {
            bloodBarAnimator.speed = 0;
            Destroy(gameObject.GetComponent<Collider>());
            Destroy(gameObject, (float)1.2);
            if (isAttack <= 0)
            {
                skeletonAnimator.SetTrigger("walk-to-death");
            }
            else
            {
                skeletonAnimator.SetTrigger("attack-to-death");
            }
        }
        else
        {
            bloodBarAnimator.speed = skeletonBodyManager.bloodBarSpeed;
            if (isAttack <= 0)
            {
                skeletonAnimator.SetTrigger("attack-to-walk");
                transform.position = transform.position + (Vector3.left * moveSpeed) * Time.deltaTime;
            }
            else
            {
                skeletonAnimator.SetTrigger("walk-to-attack");
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("phapsu-body")
        || collision.collider.CompareTag("elf-body")
        || collision.collider.CompareTag("fire-knight-body")
        || collision.collider.CompareTag("normal-knight-body")
        || collision.collider.CompareTag("castle")
        || collision.collider.CompareTag("witch"))
        {
            isAttack++;
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("phapsu-body")
        || collision.collider.CompareTag("elf-body")
        || collision.collider.CompareTag("fire-knight-body")
        || collision.collider.CompareTag("normal-knight-body")
        || collision.collider.CompareTag("castle")
        || collision.collider.CompareTag("witch"))
        {
            isAttack--;
        }
    }
}
