using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElfBodyManager : MonoBehaviour
{
    public float bloodBarDefaultSpeed = 0.8f;
    public float SkeletonDam = 0.1f;
    public float bossDam = 0.5f;
    public float bloodBarSpeed;
    public int isHurt = 0;

    private int numOfSkeleton = 0;
    private int numOfBoss = 0;


    private void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (isHurt <= 0)
        {
            bloodBarSpeed = 0;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("skeleton"))
        {
            numOfSkeleton++;
        }
        if (collision.collider.CompareTag("boss"))
        {
            numOfBoss++;
        }
        isHurt = numOfBoss + numOfSkeleton;
        bloodBarSpeed = bloodBarDefaultSpeed + bloodBarDefaultSpeed * numOfSkeleton * SkeletonDam
                                             + bloodBarDefaultSpeed * numOfBoss * bossDam;
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("skeleton"))
        {
            numOfSkeleton--;
        }
        if (collision.collider.CompareTag("boss"))
        {
            numOfBoss--;
        }
        isHurt = numOfBoss + numOfSkeleton;
        bloodBarSpeed = bloodBarDefaultSpeed + bloodBarDefaultSpeed * numOfSkeleton * SkeletonDam
                                             + bloodBarDefaultSpeed * numOfBoss * bossDam;
    }
}
