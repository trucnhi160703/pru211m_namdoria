﻿using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEditor;
using UnityEngine;
using static UnityEngine.GraphicsBuffer;

public class ElfManager : MonoBehaviour
{
    public GameObject arrow;

    private int isAttack = 0;
    private float bloodBarSpeed;
    private Animator bloodBarAnimator;
    private Animator elfAnimator;
    private BloodBarScript bloodBarScript;
    private ElfBodyManager elfBodyManager;
    private bool isSpawnArrow = false;
    private bool firstSpawn = true;

    private void Start()
    {
        //Get blood bar
        Transform bloodBar = transform.Find("HealthBar_elf");
        if (bloodBar != null)
        {
            bloodBarAnimator = bloodBar.GetComponent<Animator>();
            bloodBarSpeed = bloodBarAnimator.speed;
            bloodBarAnimator.speed = 0;
            bloodBarScript = bloodBar.GetComponent<BloodBarScript>();
        }

        //Get char
        Transform elf = transform.Find("elf");
        if (elf != null)
        {
            elfAnimator = elf.GetComponent<Animator>();
            elfBodyManager = elf.GetComponent<ElfBodyManager>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!bloodBarScript.isEmpty)
        {
            bloodBarAnimator.speed = elfBodyManager.bloodBarSpeed;
            if (isAttack > 0)
            {
                isSpawnArrow = true;
                elfAnimator.SetBool("attack", true);
                elfAnimator.SetBool("idle", false);
                if (firstSpawn)
                {
                    StartCoroutine(SpawnArrows());
                    firstSpawn = false;
                }
            }
            else
            {
                elfAnimator.SetBool("attack", false);
                elfAnimator.SetBool("idle", true);
                isSpawnArrow = false;
                firstSpawn = true;
            }
        }
        else
        {
            bloodBarAnimator.speed = 0;
            Destroy(gameObject.GetComponent<Collider>());
            elfAnimator.SetBool("death", true);
            elfAnimator.SetBool("attack", false);
            Destroy(gameObject, (float)2.2);
        }
    }

    IEnumerator SpawnArrows()
    {
        while (true)
        {
            if (isSpawnArrow)
            {
                yield return new WaitForSeconds(1.5f);
                Vector3 spawnPosition = transform.Find("elf").position;
                spawnPosition.y = spawnPosition.y - 1.0f;
                spawnPosition.x = spawnPosition.x + 1f;
                Instantiate(arrow, spawnPosition, Quaternion.identity);
            }
            else
            {
                break;
            }

        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("skeleton-body") || collision.collider.CompareTag("boss-body"))
        {
            isAttack++;
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("skeleton-body") || collision.collider.CompareTag("boss-body"))
        {
            isAttack--;
        }
    }

}
