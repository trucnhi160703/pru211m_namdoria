﻿using UnityEngine;

public class Arrow : MonoBehaviour
{
    public float moveSpeed = 5;
    private bool isTouch = false;
    private Animator arrowAni;
    private Animator elfAni;
    private Camera mainCamera;
    private AudioSource audioSource;

    private void Start()
    {
        arrowAni = GetComponent<Animator>();
        elfAni = GetComponent<Animator>();
        mainCamera = Camera.main;
        audioSource = GetComponent<AudioSource>();
    }
    void Update()
    {
        if (!isTouch)
        {
            transform.position = transform.position + (Vector3.right * moveSpeed) * Time.deltaTime;
        }
        if (!IsInCameraView())
        {
            Destroy(gameObject);
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("boss-body") || collision.collider.CompareTag("skeleton-body"))
        {
            audioSource.Play();
            isTouch = true;
            Invoke("HideArrow", 0.15f);
            Destroy(gameObject, 2.0f);
        }
    }

    private void HideArrow()
    {
        Vector3 currentPosition = transform.position;
        currentPosition.z = 9f;
        transform.position = currentPosition;
    }
    bool IsInCameraView()
    {
        Vector3 viewPos = mainCamera.WorldToViewportPoint(transform.position);
        return (viewPos.x >= 0 && viewPos.x <= 1 && viewPos.y >= 0 && viewPos.y <= 1);
    }
}
