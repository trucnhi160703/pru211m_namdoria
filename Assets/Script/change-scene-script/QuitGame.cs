using UnityEngine;

public class QuitGame : MonoBehaviour
{
    // Hàm này được gọi khi nút "Quit" được nhấn
    public void Quit()
    {
        // Thoát game
        Debug.Log("exit");
        Application.Quit();

    }
}
