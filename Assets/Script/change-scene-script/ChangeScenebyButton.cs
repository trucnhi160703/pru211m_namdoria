using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class ChangeScenebyButton : MonoBehaviour
{
    public string targetSceneName; // The name of the scene you want to load
    public Animator transition;
    public float transitionTime = 1f;
    public Button sceneChangeButton; // Reference to the UI button

    private void Start()
    {
        sceneChangeButton.onClick.AddListener(ChangeToTargetScene);
    }
    
    void ChangeToTargetScene()
    {
        SceneManager.LoadScene(targetSceneName);
    }
}
