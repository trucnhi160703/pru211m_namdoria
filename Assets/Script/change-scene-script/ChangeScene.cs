using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public class SceneSwitcher : MonoBehaviour
{
    public string targetSceneName; // Tên của Scene bạn muốn chuyển đến
    public Animator transition;
    public float transitionTime = 1f;

    private void OnMouseDown()
    {
        LoadLevel();
        SceneManager.LoadScene(targetSceneName);
    }

    IEnumerator LoadLevel()
    {
        //Play Animation
        transition.SetTrigger("Start");

        //Wait
        yield return new WaitForSeconds(transitionTime);

        //Load Scene
    }

}