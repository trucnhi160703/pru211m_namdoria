using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    private Animator animator;
    private Animator child1Animator;
    private bool isTouch = false;
    private int moveSpeed = 2;


    private void Start()
    {
        // Get a reference to the Animator component.
        animator = GetComponentInChildren<Animator>();

        Transform child1 = transform.Find("ProgressBar_9");
        if (child1 != null)
        {
            child1Animator = child1.GetComponent<Animator>();
            // Disable the Animator initially
            child1Animator.enabled = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!isTouch)
        {
            transform.position = transform.position + (Vector3.left * moveSpeed) * Time.deltaTime;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        // Check if the collider that triggered the collision has a specific tag.
        if (collision.collider.CompareTag("player"))
        {
            isTouch = true;
            //Set the "Collided" trigger to transition to a different animation state.
            //animator.SetBool("run", false);
            //animator.SetBool("death", false);
            //animator.SetBool("attack", true);
            //child1Animator.enabled = true;
        }
    }
}
